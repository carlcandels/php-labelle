<?php

	session_start();

	include_once 'classes/database.php';
	include_once 'classes/shop.php';
	include_once 'classes/item.php';

	if(isset($_SESSION['flag']) && !empty($_SESSION['flag']))
	{
		$login = $_SESSION['flag'];
	}
	else
	{
		$login = "0";
	}

	$database = new Database();
    $db = $database->getConnection();
    $flag;
                
    $shop = new Shop($db);
    $item = new Item($db);
    $stmt_item = $item->readAll();
    $stmt_item2 = $item->readAll();
    $stmt_item3= $item->readAll();
    $stmt_item4 = $item->readAll();
    $stmt_item5 = $item->readAll();
    $stmt_item6 = $item->readAll();
    $stmt_item7 = $item->readAll();
    $stmt_item8 = $item->readAll();
    $stmt_item9 = $item->readAll();
    $stmt = $shop->readLogo();
    $stmt2 = $shop->readAll();

    if($_POST)
    {
    	if($_POST['flag'] == '0')
    	{
    		include_once 'classes/users.php';
	        $users = new Users($db);

	       		$users->username = $_POST['username'];
		        $users->password = $_POST['pass'];
		        $row = $users->login();

		        if($row == 1){
		        	$_SESSION['flag'] = "1";
		        	echo "sucess";
		        	header("Refresh:0");
		        }else{
		        	echo "<div class=\"alert alert-danger alert-dismissable\">";
		                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		                echo "Failed to Login.";
		            echo "</div>";
		        }
    	}
    	else if($_POST['flag'] == '1')
    	{
    		include_once 'classes/users.php';
	        $users = new Users($db);
	        
	        if($_POST['pass'] == $_POST['confpass']){
	        	$users->lname = $_POST['lname'];
		        $users->fname = $_POST['fname'];
		        $users->mname = $_POST['mname'];
		        $users->email = $_POST['email'];
		        $users->username = $_POST['username'];
		        $users->password = $_POST['pass'];
	        }
	        	
	        

	        if($users->insert())
	        {
	            echo "<div class=\"alert alert-success alert-dismissable\">";
		            echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		            echo "Sign-up Successful.";
		        echo "</div>";
	        }
	        else
	        {
	            echo "<div class=\"alert alert-danger alert-dismissable\">";
		            echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		            echo "Sign up Failed.";
		        echo "</div>";
	        }
    	}
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>La Belle Fashion and Cafe</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/login_modal.css" rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet">

  </head>
  <body>
    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">

			<div class="row">
				<div class="col-md-12">
					 <!-- Header -->

				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					 <!-- Navigation -->
					
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					 <!-- Navigation -->

					 <ul class="nav nav-tabs">
						<li class="active">
							<a href="#">Home</a>
						</li>
						<li>
							<a href="#">Profile</a>
						</li>
						<li class="disabled">
							<a href="#">Messages</a>
						</li>
						<li class="dropdown pull-right">
							 <a href="#" data-toggle="dropdown" class="dropdown-toggle">Dropdown<strong class="caret"></strong></a>
							<ul class="dropdown-menu">

								<?php
									if($login == "0"){ 
										echo "<li>";
											echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#login-modal\">Login</a>";
										echo "</li>";
										echo "<li>";
											echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#signup-modal\">Sign-up</a>";
										echo "</li>";
										echo "<li class=\"divider\">";
										echo "</li>";
										echo "<li>";
											echo "<a href=\"#\">Customer Support</a>";
										echo "</li>";

									}
									else{ 
									
										echo "<li>";
											echo "<a href=\"user/logout.php\">Logout</a>";
										echo "</li>";
										echo "<li class=\"divider\">";
										echo "</li>";
										echo "<li>";
											echo "<a href=\"#\">Customer Support</a>";
										echo "</li>";
									}
								?>
								
							</ul>
						</li>
					</ul>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					 <!-- Category -->
					<div class="tabbable tabs-left" id="tabs-721418">
					  	<ul class="nav nav-tabs">
					  		
								
						    	<li><a href = "#tab1" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">New</a></li>
								<li><a href = "#tab2" type="button"  data-toggle="tab" class="btn btn-default" style = "width:200px;">All Items</a></li>
								<li><a href = "#tab3" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Shops</a></li>
								<li><a href = "#tab5" type="button"  data-toggle="tab" class="btn btn-default" style = "width:200px;">Accessories</a></li>
								<li><a href = "#tab6" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Tops</a></li>
								<li><a href = "#tab7" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Bottoms</a></li>
								<li><a href = "#tab8" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Footwear</a></li>
								<li><a href = "#tab9" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Coats and Jackets</a></li>
								<li><a href = "#tab10" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Swimwear</a></li>
								<li><a href = "#tab11" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Full Body Outfits</a></li>
								<li><a href = "#tab12" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Sale</a></li>
								<li><a href = "#tab13" type="button" data-toggle="tab" class="btn btn-default" style = "width:200px;">Others</a></li>
							
					  	</ul>
					  	<div class="tab-content">
					    	<div class="tab-pane active" id="tab1">
					    		<?php $count = 0;?>	
					            <?php while ($row = $stmt_item->fetch(PDO::FETCH_ASSOC)){
								 	extract($row);
								?>

									<?php if($count == 0){?>
										<div class="col-lg-2 col-md-2 col-xs-2 thumb">
							                <a class="thumbnail" href="#">
							                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
							                </a>
							            </div>
																				
									<?php }else {?>
										<div class="col-lg-2 col-md-2 col-xs-2 thumb">
							                <a class="thumbnail" href="#">
							                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
							                </a>
							            </div>
							        <?php }?>

									<?php $count=$count+1;?>
								<?php } ?>
					    		
					        </div>
					        <div class="tab-pane" id="tab2">
					            <?php $count = 0;?>	
					            <?php while ($row = $stmt2->fetch(PDO::FETCH_ASSOC)){
								 	extract($row);
								?>

									<?php if($count == 0){?>
										<div class="col-lg-2 col-md-2 col-xs-2 thumb">
							                <a class="thumbnail" href="#">
							                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Shop Images/<?php echo $shopLogo; ?>">
							                </a>
							            </div>
																					
									<?php }else {?>
										<div class="col-lg-2 col-md-2 col-xs-2 thumb">
							                <a class="thumbnail" href="#">
							                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Shop Images/<?php echo $shopLogo; ?>">
							                </a>
							            </div>
							        <?php }?>

									<?php $count=$count+1;?>
								<?php } ?>
					        </div>
					        <div class="tab-pane" id="tab3">
								<?php $count = 0;?>	
						            <?php while ($row = $stmt_item2->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '101'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>
					        </div>
					        <div class="tab-pane" id="tab4">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item3->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '102'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>
					        </div>
					        <div class="tab-pane" id="tab5">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item4->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '103'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>             
					        </div>
					        <div class="tab-pane" id="tab6">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item5->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '104'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>
					        </div>
					        <div class="tab-pane" id="tab7">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item6->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '105'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>
					        </div>
					        <div class="tab-pane" id="tab8">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item7->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category == '106'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:200px; width:200px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:200px; width:200px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>              
					        </div>
					        <div class="tab-pane" id="tab9">
					             <?php $count = 0;?>	
						            <?php while ($row = $stmt_item9->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($remarks == 'sale'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>              
					        </div>
					        <div class="tab-pane" id="tab10">
					            <?php $count = 0;?>	
						            <?php while ($row = $stmt_item8->fetch(PDO::FETCH_ASSOC)){
									 	extract($row);

									 	if($category != '101' && $category != '102' && $category != '103' && $category != '104' && $category != '105' && $category != '106'){
									?>

										<?php if($count == 0){?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
																					
										<?php }else {?>
											<div class="col-lg-2 col-md-2 col-xs-2 thumb">
								                <a class="thumbnail" href="#">
								                    <img alt="Image Preview" style = "height:140px; width:140px;" src="Item Images/<?php echo $img; ?>">
								                </a>
								            </div>
								        <?php }?>

										<?php $count=$count+1;?>
								<?php } 
								}
								?>
					        </div>
					  	</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	  <div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>Login to Your Account</h1><br>
				  <form action='index.php' method = 'post'>
				  	<?php $flag = '0'; ?>
				  	<input type= "hidden" name = "flag" value = "0">
					<input type="text" name="username" placeholder="Username">
					<input type="password" name="pass" placeholder="Password">
					<input type="submit" name="login" class="login loginmodal-submit" value="Login">
				  </form>
				</div>
			</div>
		</div>

		<div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	  <div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>Sign up</h1><br>
				  <form action='index.php' method = 'post'>
				  	<?php $flag = '1'; ?>
				  	<input type= "hidden" name = "flag" value = "1">
					<input type="text" name="lname" placeholder="Lastname" required>
					<input type="text" name="fname" placeholder="Firstname" required>
					<input type="text" name="mname" placeholder="Middlename" required>
					<input type="text" name="email" placeholder="Email" required>
					<input type="text" name="username" placeholder="Username" required>
					<input type="password" name="pass" placeholder="Password" required>
					<input type="password" name="confpass" placeholder="Confirm Password" required>
					<input type="submit" name="signup" class="login loginmodal-submit" value="Sign up">
				  </form>
				</div>
			</div>
		</div>
</div>
	<script type="text/javascript">
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>