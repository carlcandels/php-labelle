<?php
 
include_once 'classes/database.php';
include_once 'classes/category.php';
include_once 'classes/users.php';
$database = new Database();
$db = $database->getConnection();

// array for JSON response
$response = array();
$category = new Category($db);    
$users = new Users($db);
 
// check for required fields
if ($_POST) {
    //instantiate
    $categoryID= $_POST['categoryID'];
    $categoryName   = $_POST['categoryName'];
    $flag = $_POST['flag'];

    //**********************************************
    $users->categoryID = $categoryID;
    $users->categoryName = $categoryName;
    $users->flag = $flag;

    $isExisting = $category->isExisting();

    if($isExisting)
    {
        $category->update();
        $response["success"] = 1;
        $response["message"] = "Success UPDATE.";
    }
    else
    {
        $category->insert();    
        $response["success"] = 1;
        $response["message"] = "Success INSERT";
    }
    echo json_encode($response);
}
?>