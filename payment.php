<?php
use PayPal\Api\Payer;
use PayPal\Api\Details;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;

require '../src/start.php';
include_once '../classes/database2.php';
include_once '../classes/order.php';
include_once '../classes/orderbatch.php';

$database = new Database();
$db = $database->getConnection();

$order = new Order($db);
$orderbatch = new OrderBatch($db);

$stack2 = array();

$stack = isset($_SESSION['paypal_pay']) ? $_SESSION['paypal_pay'] : null;


$subtotal = $stack[0];
$shipping_fee = $stack[1];
$amountDue = $stack[2];
$dateNow = date("m/d/Y h:i:sa");
$flag = "1";
$userID = $_SESSION['user_id'];


$payer = new Payer();
$details = new Details();
$amount = new Amount();
$transaction = new Transaction();
$payment = new Payment();
$redirectUrls = new RedirectUrls();







//Payer
$payer->setPaymentMethod('paypal');

//Details
$details->setShipping($shipping_fee)
	->setTax('0.00')
	->setSubtotal($subtotal);

//Amount
$amount->setCurrency('PHP')
	->setTotal($amountDue)
	->setDetails($details);
//Transaction
$transaction->setAmount($amount)
	->setDescription('Order Payment');

//Payment
$payment->setIntent('sale')
	->setPayer($payer)
	->setTransactions([$transaction]);

//Redirect Urls
$redirectUrls->setReturnUrl('http://192.168.43.152/LaBelle/paypal/pay.php?approved=true')
	->setCancelUrl('http://192.168.43.152/LaBelle/paypal/pay.php?approved=false');

$payment->setRedirectUrls($redirectUrls);

try{
	$payment->create($api);

	//Generate and store hash
	$hash = md5($payment->getId());
	$_SESSION['payment_id'] = $payment->getId();
	$_SESSION['paypal_hash'] = $hash;

}catch(PPConnectionException $e){
	header('Location: ../paypal/error.php');
}
foreach ($payment->getLinks() as $link) {
	if ($link->getRel()=='approval_url') {
		$redirectUrl = $link->getHref();
	}
}

header('Location: ' . $redirectUrl);
?>