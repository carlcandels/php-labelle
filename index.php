<?php

    session_start();

    include_once 'classes/database.php';
    include_once 'classes/shop.php';
    include_once 'classes/item.php';
    include_once 'classes/order.php';


    if(isset($_SESSION['flag']) && !empty($_SESSION['flag']))
    {
        $login = $_SESSION['flag'];
    }
    else
    {
        $login = "0";
    }

    $image_id = null;
    $database = new Database();
    $db = $database->getConnection();
    $flag;
                
    $shop = new Shop($db);
    $item = new Item($db);

    $stmt_item = $item->readAll();
    $stmt_item2 = $item->readAll();
    $stmt_item3= $item->readAll();
    $stmt_item4 = $item->readAll();
    $stmt_item5 = $item->readAll();
    $stmt_item6 = $item->readAll();
    $stmt_item7 = $item->readAll();
    $stmt_item8 = $item->readAll();
    $stmt_item9 = $item->readAll();
    $stmt_item10 = $item->readAll();
    $stmt_item11 = $item->readAll();
    $stmt_item12 = $item->readAll();
    $stmt = $shop->readLogo();
    $stmt2 = $shop->readAll();

    if($_POST)
    {
        if($_POST['flag'] == '0')
        {
            include_once 'classes/users.php';
            $users = new Users($db);

                $users->username = $_POST['username'];
                $users->password = $_POST['pass'];
                $row = $users->login();

                if($row == 1){
                    $_SESSION['flag'] = "1";
                    echo "sucess";
                    header("Refresh:0");
                    $users->getUserID();
                    $_SESSION['user_id'] = $users->userID;

                }else{
                    echo "<div class=\"alert alert-danger alert-dismissable\">";
                        echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                        echo "Failed to Login.";
                    echo "</div>";
                }
        }
        else if($_POST['flag'] == '1')
        {
            include_once 'classes/users.php';
            $users = new Users($db);
            
            if($_POST['pass'] == $_POST['confpass']){
                $users->lname = $_POST['lname'];
                $users->fname = $_POST['fname'];
                $users->mname = $_POST['mname'];
                $users->email = $_POST['email'];
                $users->username = $_POST['username'];
                $users->password = $_POST['pass'];
            }
                
            

            if($users->insert())
            {
                echo "<div class=\"alert alert-success alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "Sign-up Successful.";
                echo "</div>";
            }
            else
            {
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "Sign up Failed.";
                echo "</div>";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>La Belle Fashion and Cafe</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/jquery.tabpager.css"/>
    

    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.tabpager.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet">
    <link href="css/login_modal.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">

    <link href="css/personalizedCss.css" rel="stylesheet">


    


  </head>
  <body>

    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                     
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                         <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button> <a class="navbar-brand" href="#">La Belle Fashion and Cafe</a>
                </div>
                
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                        <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                        <li>
                            <a href="user/cart.php">Cart</a>
                        </li>
                       
                        <?php

                        if($login!="0")
                        {
                            echo "<li>";
                                echo"<a href='user/orderlist.php'>Orders</a>";
                            echo "</li>";
                        }
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a id = "dropdown" href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">

                                <?php
                                    if($login == "0"){ 
                                        echo "<li>";
                                            echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#login-modal\">Login</a>";
                                        echo "</li>";
                                        echo "<li>";
                                            echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#signup-modal\">Sign-up</a>";
                                        echo "</li>";
                                        echo "<li class=\"divider\">";
                                        echo "</li>";
                                        echo "<li>";
                                            echo "<a href=\"#\">Customer Support</a>";
                                        echo "</li>";

                                    }
                                    else{ 
                                    
                                        echo "<li>";
                                            echo "<a href=\"user/logout.php\">Logout</a>";
                                        echo "</li>";
                                        echo "<li class=\"divider\">";
                                        echo "</li>";
                                        echo "<li>";
                                            echo "<a href=\"#\">Customer Support</a>";
                                        echo "</li>";
                                    }
                                ?>
                                
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </nav>
            
            <div class="carousel slide" id="carousel-13382">
                <ol class="carousel-indicators">
                    <li data-slide-to="0" data-target="#carousel-13382">
                    </li>
                    <li data-slide-to="1" data-target="#carousel-13382">
                    </li>
                    <li data-slide-to="2" data-target="#carousel-13382" class="active">
                    </li>
                </ol>
                <div class="carousel-inner">
                    <div class="item">
                        <center>
                        <img alt="Carousel Bootstrap First" src="images/b1.jpg" />
                        </center>
                        <div class="carousel-caption">
                            <h4>
                                First Thumbnail label
                            </h4>
                            <p>
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <center>
                        <img alt="Carousel Bootstrap Second" src="images/b2.jpg" />
                        </center>
                        <div class="carousel-caption">
                            <h4>
                                Second Thumbnail label
                            </h4>
                            <p>
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                            </p>
                        </div>
                    </div>
                    <div class="item active">
                        <center>
                        <img alt="Carousel Bootstrap Third" src="images/b3.jpg" />
                        </center>
                        <div class="carousel-caption">
                            <h4>
                                Third Thumbnail label
                            </h4>
                            <p>
                                Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                            </p>
                        </div>
                    </div>
                </div> <a class="left carousel-control" href="#carousel-13382" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-13382" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            
                <h1>
                    La Belle Fille <small>The first ever showroom with a cafe under one roof!</small>
                </h1>
            
        </div>

            <!--<div class="page-header">
                <h1>
                   
                </h1>
            </div> -->
        </div>
        </div>
    </div>
    <div class="col-md-12">
    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <center>
                   
                        <ul class="nav nav-tabs" id="tab">
                            <li type="button" class="btn btn-info btn-lg btn-block">All Items</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Shops</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Accessories</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Tops</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Bottoms</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Footwear</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Coats and Jackets</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Swimwear</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Full Body Outfits</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Sale</li>
                            <li type="button" class="btn btn-info btn-lg btn-block">Others</li>
                                    
                        </ul>
                   
                </center>
            </div>
        </div>
        
        <div class="col-md-9">
            <div class="row">
                <div id="wrapper">
                    <div class="contents">
                          <?php $count = 0;?> 
                          <?php while ($row = $stmt_item->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                          ?>
                              <?php if($remQty2 != 0){
                              if($count==0){?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                      <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                      <p>Name: <?php echo $itemDesc; ?></p>
                                                      <p>Qty: <?php echo $remQty2; ?></p>
                                                      <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>
                                                           
                                          </a>
                                    </div>
                              <?php }else {?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                    <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                    <p>Name: <?php echo $itemDesc; ?></p>
                                                    <p>Qty: <?php echo $remQty2; ?></p>
                                                    <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>                
                                          </a>
                                    </div>
                              <?php }
                              }?>

                              <?php $count=$count+1;?>
                          <?php } ?>
                      </div>
                      <div class="contents">
                          <?php $count = 0;?> 
                          <?php while ($row = $stmt_item2->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                          ?>
                              <?php if($remQty2 != 0){
                              if($count==0){?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                      <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                      <p>Name: <?php echo $itemDesc; ?></p>
                                                      <p>Qty: <?php echo $remQty2; ?></p>
                                                      <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>
                                                           
                                          </a>
                                    </div>
                              <?php }else {?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                    <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                    <p>Name: <?php echo $itemDesc; ?></p>
                                                    <p>Qty: <?php echo $remQty2; ?></p>
                                                    <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>                
                                          </a>
                                    </div>
                              <?php }
                              }?>

                              <?php $count=$count+1;?>
                          <?php } ?>
                      </div>
                      <div class="contents">
                          <?php $count = 0;?> 
                          <?php while ($row = $stmt_item3->fetch(PDO::FETCH_ASSOC)){
                                extract($row);
                          ?>
                              <?php if($remQty2 != 0){
                              if($count==0){?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                      <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                      <p>Name: <?php echo $itemDesc; ?></p>
                                                      <p>Qty: <?php echo $remQty2; ?></p>
                                                      <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>
                                                           
                                          </a>
                                    </div>
                              <?php }else {?>
                                    <div class="col-sm-4 col-lg-4 col-md-4">
                                          <a class="thumbnail" href = "user/item.php?itemCode=<?php echo $itemCode;?>">
                                                <img id = "<?php echo $itemCode; ?>" src="Item Images/<?php echo $img; ?>" alt="Image Preview" style = "height:140px; width:430px;">
                                                <div class="caption">
                                                    <h4 class="pull-right">PHP <?php echo $retailPrice; ?></h4>
                                                    <p>Name: <?php echo $itemDesc; ?></p>
                                                    <p>Qty: <?php echo $remQty2; ?></p>
                                                    <p>Item Code: <?php echo $itemCode; ?></p>
                                                </div>                
                                          </a>
                                    </div>
                              <?php }
                              }?>

                              <?php $count=$count+1;?>
                          <?php } ?>
                      </div>
                </div>
            </div>

        </div>
    </div>
    
    <center>
</div>
<div id = "footer" class="col-md-12">
    

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1>Login to Your Account</h1><br>
                  <form action='index.php' method = 'post'>
                    <?php $flag = '0'; ?>
                    <input type= "hidden" name = "flag" value = "0">
                    <input type="text" name="username" placeholder="Username">
                    <input type="password" name="pass" placeholder="Password">
                    <input type="submit" name="login" class="login loginmodal-submit" value="Login">
                  </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1>Sign up</h1><br>
                  <form action='index.php' method = 'post'>
                    <?php $flag = '1'; ?>
                    <input type= "hidden" name = "flag" value = "1">
                    <input type="text" name="lname" placeholder="Lastname" required>
                    <input type="text" name="fname" placeholder="Firstname" required>
                    <input type="text" name="mname" placeholder="Middlename" required>
                    <input type="text" name="email" placeholder="Email" required>
                    <input type="text" name="username" placeholder="Username" required>
                    <input type="password" name="pass" placeholder="Password" required>
                    <input type="password" name="confpass" placeholder="Confirm Password" required>
                    <input type="submit" name="signup" class="login loginmodal-submit" value="Sign up">
                  </form>
                </div>
            </div>
        </div>
</div>
</div>

    

<style>*/
    html{
        position:relative;
    }  
    body{
        height: auto;
        min-height:100%;
    } 

    #wrapper {
      border: 0px solid;
      padding: 10px;
      min-height: 100%;
      position:absolute;    
      height: calc(100% + 20)
    }

    .contents { 
      margin-bottom: 10px; 
      display: block;
      min-height: 100%;
    }

    .jquery-tab-pager-tabbar li.current { color: #fff; }

    #tab li.current { font-style: italic; }

    #jquery-tab-pager-navi { margin-bottom: 10px; }


    * { font-family: 'Roboto', Helvetica, sans-serif; }

    
  </style>

  <script>
      $(document).ready(function() {

        var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;

            trigger.click(function () {
              hamburger_cross();      
            });

            function hamburger_cross() {

              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }
          
          $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
          });  

          $('[data-toggle="dropdown"]').click(function () {
                $('#dropdown').toggleClass('dropdown-toggle');
          }); 




          $("#tab").tabpager({

              //  maximum visible items
              items: 9,

              // CSS class for tabbed content
              contents: 'contents',

              // transition speed
              time: 300,

              // text for previous button
              previous: '&laquo;Prev',

              // text for next button
              next: 'Next&raquo;',

              // initial tab
              start: 1,

              // top or bottom
              position: 'top',

              // scrollable
              scroll: true
          });
      });
  </script>

   <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.min.js"></script>
    <script src="js/jquery.tabpager.min.js"></script>
</html>