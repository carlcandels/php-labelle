<?php

    session_start();

    include_once 'classes/database.php';
    include_once 'classes/shop.php';
    include_once 'classes/item.php';
    include_once 'classes/order.php';


    if(isset($_SESSION['flag']) && !empty($_SESSION['flag']))
    {
        $login = $_SESSION['flag'];
    }
    else
    {
        $login = "0";
    }

    $image_id = null;
    $database = new Database();
    $db = $database->getConnection();
    $flag;
                
    $shop = new Shop($db);
    $item = new Item($db);

    $stmt_item = $item->readAll();
    $stmt_item2 = $item->readAll();
    $stmt_item3= $item->readAll();
    $stmt_item4 = $item->readAll();
    $stmt_item5 = $item->readAll();
    $stmt_item6 = $item->readAll();
    $stmt_item7 = $item->readAll();
    $stmt_item8 = $item->readAll();
    $stmt_item9 = $item->readAll();
    $stmt_item10 = $item->readAll();
    $stmt_item11 = $item->readAll();
    $stmt_item12 = $item->readAll();
    $stmt = $shop->readLogo();
    $stmt2 = $shop->readAll();

    if($_POST)
    {
        if($_POST['flag'] == '0')
        {
            include_once 'classes/users.php';
            $users = new Users($db);

                $users->username = $_POST['username'];
                $users->password = $_POST['pass'];
                $row = $users->login();

                if($row == 1){
                    $_SESSION['flag'] = "1";
                    echo "sucess";
                    header("Refresh:0");
                    $users->getUserID();
                    $_SESSION['user_id'] = $users->userID;

                }else{
                    echo "<div class=\"alert alert-danger alert-dismissable\">";
                        echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                        echo "Failed to Login.";
                    echo "</div>";
                }
        }
        else if($_POST['flag'] == '1')
        {
            include_once 'classes/users.php';
            $users = new Users($db);
            
            if($_POST['pass'] == $_POST['confpass']){
                $users->lname = $_POST['lname'];
                $users->fname = $_POST['fname'];
                $users->mname = $_POST['mname'];
                $users->email = $_POST['email'];
                $users->username = $_POST['username'];
                $users->password = $_POST['pass'];
            }
                
            

            if($users->insert())
            {
                echo "<div class=\"alert alert-success alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "Sign-up Successful.";
                echo "</div>";
            }
            else
            {
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "Sign up Failed.";
                echo "</div>";
            }
        }
    }

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>La Belle Fashion and Cafe</title>
	<!-- CSS -->
	<link rel="stylesheet" href="css/jquery.tabpager.css"/>
	

	<!-- JS -->
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.tabpager.min.js"></script>
	<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/shop-homepage.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/navbar.css" rel="stylesheet">
  <link href="css/login_modal.css" rel="stylesheet">

	</head>


	<body>
    <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
              <!-- Navigation -->
              <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                  <div class="container">
                      <!-- Brand and toggle get grouped for better mobile display -->
                      <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="#">La Belle Fashion and Cafe</a>
                      </div>
                      <!-- Collect the nav links, forms, and other content for toggling -->
                      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          <ul class="nav navbar-nav">
                              <li>
                                  <a href="index.php">Home</a>
                              </li>
                              <li>
                                  <a href="#">Services</a>
                              </li>
                              <li>
                                  <a href="#">Contact</a>
                              </li>
                              <li>
                                  <a href="user/cart.php">Cart</a>
                              </li>
                             
                              <?php

                              if($login!="0")
                              {
                                  echo "<li>";
                                      echo"<a href='user/orderlist.php'>Orders</a>";
                                  echo "</li>";
                              }
                              ?>
                          </ul>
                              <ul class="nav navbar-nav navbar-right">
                              <li class="dropdown">
                                       <a id = "dropdown" href="#" data-toggle="dropdown" class="dropdown-toggle">Dropdown<strong class="caret"></strong></a>
                                      <ul class="dropdown-menu">

                                          <?php
                                              if($login == "0"){ 
                                                  echo "<li>";
                                                      echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#login-modal\">Login</a>";
                                                  echo "</li>";
                                                  echo "<li>";
                                                      echo"<a href=\"#\" data-toggle=\"modal\" data-target=\"#signup-modal\">Sign-up</a>";
                                                  echo "</li>";
                                                  echo "<li class=\"divider\">";
                                                  echo "</li>";
                                                  echo "<li>";
                                                      echo "<a href=\"#\">Customer Support</a>";
                                                  echo "</li>";

                                              }
                                              else{ 
                                              
                                                  echo "<li>";
                                                      echo "<a href=\"user/logout.php\">Logout</a>";
                                                  echo "</li>";
                                                  echo "<li class=\"divider\">";
                                                  echo "</li>";
                                                  echo "<li>";
                                                      echo "<a href=\"#\">Customer Support</a>";
                                                  echo "</li>";
                                              }
                                          ?>
                                          
                                      </ul>
                                  </li>
                                </ul>
                            </ul>
                      </div>
                      <!-- /.navbar-collapse -->
                  </div>
                  <!-- /.container -->
              </nav>
              <!-- /.navigation -->
        </div>
      </div>
      <!--end of col-md-12-->
    </div>
      

    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1>Login to Your Account</h1><br>
                  <form action='index.php' method = 'post'>
                    <?php $flag = '0'; ?>
                    <input type= "hidden" name = "flag" value = "0">
                    <input type="text" name="username" placeholder="Username">
                    <input type="password" name="pass" placeholder="Password">
                    <input type="submit" name="login" class="login loginmodal-submit" value="Login">
                  </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1>Sign up</h1><br>
                  <form action='index.php' method = 'post'>
                    <?php $flag = '1'; ?>
                    <input type= "hidden" name = "flag" value = "1">
                    <input type="text" name="lname" placeholder="Lastname" required>
                    <input type="text" name="fname" placeholder="Firstname" required>
                    <input type="text" name="mname" placeholder="Middlename" required>
                    <input type="text" name="email" placeholder="Email" required>
                    <input type="text" name="username" placeholder="Username" required>
                    <input type="password" name="pass" placeholder="Password" required>
                    <input type="password" name="confpass" placeholder="Confirm Password" required>
                    <input type="submit" name="signup" class="login loginmodal-submit" value="Sign up">
                  </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="item-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
                <div class="loginmodal-container">
                    
                    <img src = "Item Images/<?php echo $image_id;?>" style = "width: 140px; height: 140px;" id="imageid">
                    <p>add to cart goes here! <span class='imageid'></span></p>
                    <?php echo $image_id;?>
                </div>
            </div>
        </div>
    <!-- /.container -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
          

        });
    </script>    

   

  <style>*/
    .jquery-tab-pager-tabbar li.current { color: #fff; }

    #tab li.current { font-style: italic; }

    #jquery-tab-pager-navi { margin-bottom: 10px; }


    * { font-family: 'Roboto', Helvetica, sans-serif; }

    #wrapper {
      border: 1px solid #eee;
      padding: 10px;
    }

    .contents { 
      margin-bottom: 10px; 
      display: block;
      height: 900px;
    }
  </style>

  <script>
      $(document).ready(function() {

        var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;

            trigger.click(function () {
              hamburger_cross();      
            });

            function hamburger_cross() {

              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }
          
          $('[data-toggle="offcanvas"]').click(function () {
                $('#wrapper').toggleClass('toggled');
          });  

          $('#dropdown').click(function(){
            $('.dropdown-menu').toggle();
          })

          $("#tab").tabpager({

              //  maximum visible items
              items: 9,

              // CSS class for tabbed content
              contents: 'contents',

              // transition speed
              time: 300,

              // text for previous button
              previous: '&laquo;Prev',

              // text for next button
              next: 'Next&raquo;',

              // initial tab
              start: 1,

              // top or bottom
              position: 'bottom',

              // scrollable
              scroll: true
          });
      });
  </script>

   <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/script.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.tabpager.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>