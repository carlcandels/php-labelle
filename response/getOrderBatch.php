<?php
 
// array for JSON response
$response = array();

include_once '../classes/database2.php';
include_once '../classes/orderBatch.php';

$database = new Database();
$db = $database->getConnection();

$orderbatch = new OrderBatch($db);

if($_POST)
{
    $orderbatchID = $_POST['orderBatchID'];
    $orderbatch->orderbatchID = $orderbatchID;
    $stmt_readOne = $orderbatch->readOne();

    $count = 0;
    
    
        $response['orderBatchID'] = $orderbatchID;
        $response['orderID'] = $orderbatch->orderID;
        $response['itemCode'] = $orderbatch->itemCode;
        $response['qty'] =$orderbatch->qty;
        $response['price'] = $orderbatch->price;


    echo json_encode($response);
}else{
    $response['success'] = 0;
    $response['message'] = "Its not working.";

    echo json_encode($response);
}
?>