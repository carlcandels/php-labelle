-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 06:26 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `labelle_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemCode` varchar(50) NOT NULL,
  `itemDesc` text NOT NULL,
  `retailPrice` double NOT NULL,
  `qty` int(11) NOT NULL,
  `dateDelivered` varchar(50) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `shopID` varchar(50) NOT NULL,
  `remQty` int(11) NOT NULL,
  `category` text NOT NULL,
  `img` varchar(300) NOT NULL,
  `keywords` text NOT NULL,
  `remQty2` int(11) NOT NULL,
  `color` text NOT NULL,
  `size` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemCode`, `itemDesc`, `retailPrice`, `qty`, `dateDelivered`, `remarks`, `shopID`, `remQty`, `category`, `img`, `keywords`, `remQty2`, `color`, `size`) VALUES
('LBFC10200001', 'Slim Fit Jeans', 187.5, 10, '11/13/2015', 'Sale', '101', 30, '101', '1.jpg', '', 26, 'White', '32'),
('LBFC10200002', 'Slim Fit Shirt', 200, 15, '11/13/2015', '', '101', 42, '102', '2.jpg', '', 40, 'Green', 'S'),
('LBFC10200003', 'Crop top', 500, 20, '11/13/2015', '', '101', 75, '102', '3.jpg', '', 75, 'Floral', 'S'),
('LBFC10200004', 'Sweat Shirt Stripes', 500, 1, '11/13/2015', '', '101', 0, '102', '4.jpg', '', 0, 'Blue, Black', 'M'),
('LBFC10200005', 'Sweat Shirt Plain', 500, 1, '11/13/2015', '', '101', 0, '102', '5.jpg', '', 0, 'Red', 'M'),
('LBFC10200006', 'Plain Skirt', 500, 3, '11/13/2015', '', '101', 18, '102', '6.jpg', '', 18, 'Black', 'M'),
('LBFC10200007', 'Plain V-Neck', 500, 1, '11/13/2015', '', '101', 0, '102', '7.jpg', '', 0, 'Maroon', 'M'),
('LBFC10200008', 'Fit Short', 500, 1, '11/13/2015', '', '101', 0, '101', '8.jpg', '', 0, 'Brown', 'M'),
('LBFC10200009', 'Jogger Pants', 500, 1, '11/13/2015', '', '101', 0, '101', '9.jpg', '', 0, 'Brown', 'M'),
('LBFC10200010', 'Plain Long Sleeves', 500, 3, '11/13/2015', '', '101', 12, '101', '10.jpg', '', 12, 'Blue', 'M'),
('LBFC10200011', 'Plain Loose Shirt', 500, 1, '11/13/2015', '', '101', 0, '101', '11.jpg', '', 0, 'White', 'M'),
('LBFC10200012', 'Zipper Jacket', 500, 1, '11/13/2015', '', '101', 0, '101', '12.jpg', '', 0, 'Gray', 'M'),
('LBFC10200013', 'Slocks', 500, 1, '11/13/2015', '', '101', 0, '101', '13.jpg', '', 0, 'Black', 'M'),
('LBFC10200014', 'Plain Sweat Shirt ', 500, 3, '11/13/2015', '', '101', 12, '101', '14.jpg', '', 12, 'Gray', 'M'),
('LBFC10200015', 'Hoody Jacket', 500, 1, '11/13/2015', '', '101', 0, '102', '15.jpg', '', 0, 'Blue', 'M'),
('LBFC10600001', 'Slim Fit Jeans', 250, 10, '11/13/2015', '', '101', 45, '101', '1.jpg', '', 45, 'White', '32'),
('LBFC10600002', 'Slim Fit Shirt', 200, 15, '11/13/2015', '', '101', 72, '102', '2.jpg', '', 72, 'Green', 'S'),
('LBFC10600003', 'Crop top', 500, 20, '11/13/2015', '', '101', 90, '102', '3.jpg', '', 90, 'Floral', 'S'),
('LBFC10600004', 'Sweat Shirt Stripes', 500, 1, '11/13/2015', '', '101', 0, '102', '4.jpg', '', 0, 'Blue, Black', 'M'),
('LBFC10600005', 'Sweat Shirt Plain', 500, 1, '11/13/2015', '', '101', 0, '102', '5.jpg', '', 0, 'Red', 'M'),
('LBFC10600006', 'Plain Skirt', 500, 3, '11/13/2015', '', '101', 18, '102', '6.jpg', '', 18, 'Black', 'M'),
('LBFC10600007', 'Plain V-Neck', 500, 1, '11/13/2015', '', '101', 0, '102', '7.jpg', '', 0, 'Maroon', 'M'),
('LBFC10600008', 'Fit Short', 500, 1, '11/13/2015', '', '101', 0, '101', '8.jpg', '', 0, 'Brown', 'M'),
('LBFC10600009', 'Jogger Pants', 500, 1, '11/13/2015', '', '101', 0, '101', '9.jpg', '', 0, 'Brown', 'M'),
('LBFC10600010', 'Plain Long Sleeves', 500, 3, '11/13/2015', '', '101', 18, '101', '10.jpg', '', 18, 'Blue', 'M'),
('LBFC10600011', 'Plain Loose Shirt', 500, 1, '11/13/2015', '', '101', 0, '101', '11.jpg', '', 0, 'White', 'M'),
('LBFC10600012', 'Zipper Jacket', 500, 1, '11/13/2015', '', '101', 0, '101', '12.jpg', '', 0, 'Gray', 'M'),
('LBFC10600013', 'Slocks', 500, 1, '11/13/2015', '', '101', 0, '101', '13.jpg', '', 0, 'Black', 'M'),
('LBFC10600014', 'Plain Sweat Shirt ', 500, 3, '11/13/2015', '', '101', 18, '101', '14.jpg', '', 18, 'Gray', 'M'),
('LBFC10600015', 'Hoody Jacket', 500, 1, '11/13/2015', '', '101', 0, '102', '15.jpg', '', 0, 'Blue', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
`orderID` int(11) NOT NULL,
  `orderDate` varchar(50) NOT NULL,
  `totalAmount` double NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `amountTend` double NOT NULL,
  `userID` int(11) NOT NULL,
  `paymentID` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `complete` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_batch`
--

CREATE TABLE IF NOT EXISTS `order_batch` (
`orderbatchID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `itemCode` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_batch`
--

INSERT INTO `order_batch` (`orderbatchID`, `orderID`, `itemCode`, `qty`, `price`) VALUES
(1, 1, 'LBFC10200001', 2, 187.5),
(2, 1, 'LBFC10200002', 2, 200),
(3, 2, 'LBFC10200002', 2, 200),
(4, 2, 'LBFC10200003', 2, 500),
(5, 3, 'LBFC10200010', 1, 500),
(6, 3, 'LBFC10200014', 1, 500),
(7, 0, 'LBFC10200001', 3, 187.5),
(8, 4, 'LBFC10200001', 1, 187.5),
(9, 4, 'LBFC10200002', 2, 200);

-- --------------------------------------------------------

--
-- Table structure for table `order_online`
--

CREATE TABLE IF NOT EXISTS `order_online` (
`orderID` int(11) NOT NULL,
  `orderDate` varchar(30) NOT NULL,
  `totalAmount` double NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `amountTend` double NOT NULL,
  `userID` int(11) NOT NULL,
  `paymentID` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `complete` tinyint(4) NOT NULL,
  `shipping_fee` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_online`
--

INSERT INTO `order_online` (`orderID`, `orderDate`, `totalAmount`, `flag`, `amountTend`, `userID`, `paymentID`, `hash`, `complete`, `shipping_fee`) VALUES
(1, '02/18/2016 06:52:54pm', 825, 1, 825, 11, 'PAY-1A460698RW556110JK3DAJOA', '1939fb82c04611b10c73deb7a87b5973', 1, 0),
(2, '02/19/2016 12:12:02pm', 1450, 1, 1450, 11, 'PAY-02C895421F2261601K3DPP7A', 'f7764104829e9b70296e5ae85c18ce91', 1, 0),
(3, '02/19/2016 01:10:11pm', 1050, 1, 1050, 11, 'PAY-53P29527FD227825RK3DQLZA', '4fe153083b8fa65f4ff831f6f26f2b0d', 1, 0),
(4, '02/19/2016 10:28:00pm', 746.5, 1, 746.5, 11, 'PAY-29V13179MU651982FK3DYQ5Q', '5312101f717100c701db2e331ab0bfab', 1, 159);

-- --------------------------------------------------------

--
-- Table structure for table `refregion`
--

CREATE TABLE IF NOT EXISTS `refregion` (
`id` int(11) NOT NULL,
  `psgcCode` varchar(255) DEFAULT NULL,
  `regDesc` text,
  `regCode` varchar(255) DEFAULT NULL,
  `shipping_fee` double NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `refregion`
--

INSERT INTO `refregion` (`id`, `psgcCode`, `regDesc`, `regCode`, `shipping_fee`) VALUES
(1, '010000000', 'REGION I (ILOCOS REGION)', '01', 99),
(2, '020000000', 'REGION II (CAGAYAN VALLEY)', '02', 80),
(3, '030000000', 'REGION III (CENTRAL LUZON)', '03', 99),
(4, '040000000', 'REGION IV-A (CALABARZON)', '04', 90),
(5, '170000000', 'REGION IV-B (MIMAROPA)', '17', 99),
(6, '050000000', 'REGION V (BICOL REGION)', '05', 50),
(7, '060000000', 'REGION VI (WESTERN VISAYAS)', '149', 50),
(8, '070000000', 'REGION VII (CENTRAL VISAYAS)', '07', 159),
(9, '080000000', 'REGION VIII (EASTERN VISAYAS)', '08', 139),
(10, '090000000', 'REGION IX (ZAMBOANGA PENINSULA)', '09', 129),
(11, '100000000', 'REGION X (NORTHERN MINDANAO)', '10', 169),
(12, '110000000', 'REGION XI (DAVAO REGION)', '11', 159),
(13, '120000000', 'REGION XII (SOCCSKSARGEN)', '12', 169),
(14, '130000000', 'NATIONAL CAPITAL REGION (NCR)', '13', 99),
(15, '140000000', 'CORDILLERA ADMINISTRATIVE REGION (CAR)', '14', 99),
(16, '150000000', 'AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)', '15', 159),
(17, '160000000', 'REGION XIII (Caraga)', '16', 139);

-- --------------------------------------------------------

--
-- Table structure for table `ship_address`
--

CREATE TABLE IF NOT EXISTS `ship_address` (
`id` int(11) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `com_address` text NOT NULL,
  `region` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `regionID` varchar(30) NOT NULL,
  `orderID` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship_address`
--

INSERT INTO `ship_address` (`id`, `fullname`, `com_address`, `region`, `email`, `regionID`, `orderID`) VALUES
(1, 'Candelaria, Cesar Carlo B.', 'askldj', 'AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)', 'asjhaklsd@gmail.com', '16', '4');

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE IF NOT EXISTS `shop` (
  `shopID` varchar(100) NOT NULL,
  `shopName` varchar(100) NOT NULL,
  `shopAddress` text NOT NULL,
  `shopContact` varchar(20) NOT NULL,
  `shopLogo` text NOT NULL,
  `flag` int(11) NOT NULL,
  `shopOwnerID` varchar(60) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`shopID`, `shopName`, `shopAddress`, `shopContact`, `shopLogo`, `flag`, `shopOwnerID`, `email`) VALUES
('101', '101', 'General Luna St., Naga City', ' 0923882002', 'La belle Fashion and cafe.jpg', 1, '1001', 'labellefille@gmail.com'),
('102', '102', 'Anonymous', ' 234234234', 'anonymous_profile.jpg', 1, '1001', 'Anonymous@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `shopowner`
--

CREATE TABLE IF NOT EXISTS `shopowner` (
`id` int(11) NOT NULL,
  `shopOwnerID` varchar(20) NOT NULL,
  `lname` varchar(60) NOT NULL,
  `fname` varchar(60) NOT NULL,
  `mname` varchar(60) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` varchar(30) NOT NULL,
  `contactNumber` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `addr` text NOT NULL,
  `flag` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shopowner`
--

INSERT INTO `shopowner` (`id`, `shopOwnerID`, `lname`, `fname`, `mname`, `gender`, `birthdate`, `contactNumber`, `email`, `addr`, `flag`) VALUES
(3, '1001', 'Fuentebella', 'Veronica', ' M', 'M ', ' 6/21/1984', ' 09129328832', ' fuetebellanica@gmail.com', ' Urban Poor, Naga City', '1'),
(4, '1002', 'Anonymous', 'Anonymous', ' Anonymous', 'M ', ' 8/18/1900', ' 0223903', ' Anonymous@gmail.com', ' AnonymousAnonymousAnonymousAnonymous', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`userID` int(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `lname`, `fname`, `mname`, `email`, `username`, `password`) VALUES
(11, 'Candelaria', 'Cesar Carlo', 'Bartolome', 'carlcandels24@gmail.com', 'carlcandels', 'carlcandels');

-- --------------------------------------------------------

--
-- Table structure for table `users_admin`
--

CREATE TABLE IF NOT EXISTS `users_admin` (
  `userID` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `userpass` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `addr` text NOT NULL,
  `contact` varchar(100) NOT NULL,
  `restriction` varchar(100) NOT NULL,
  `flag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD UNIQUE KEY `itemCode` (`itemCode`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
 ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `order_batch`
--
ALTER TABLE `order_batch`
 ADD PRIMARY KEY (`orderbatchID`);

--
-- Indexes for table `order_online`
--
ALTER TABLE `order_online`
 ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `refregion`
--
ALTER TABLE `refregion`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship_address`
--
ALTER TABLE `ship_address`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
 ADD PRIMARY KEY (`shopID`);

--
-- Indexes for table `shopowner`
--
ALTER TABLE `shopowner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`userID`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_admin`
--
ALTER TABLE `users_admin`
 ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_batch`
--
ALTER TABLE `order_batch`
MODIFY `orderbatchID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order_online`
--
ALTER TABLE `order_online`
MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `refregion`
--
ALTER TABLE `refregion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ship_address`
--
ALTER TABLE `ship_address`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shopowner`
--
ALTER TABLE `shopowner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `userID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
