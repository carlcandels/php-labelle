<?php
 
// array for JSON response
$response = array();

include_once 'classes/database.php';
include_once 'classes/shop.php';
include_once 'classes/item.php';

$database = new Database();
$db = $database->getConnection();

$item = new Item($db);



if($_POST)
{
	$itemCode = $_POST['itemCode'];
	$item->itemCode = $itemCode;
	$stmt_readOne = $item->readOne();

	$count = 0;
	
	
		$response['itemCode'] = $itemCode;
		$response['retailPrice'] = $item->retailPrice;
		$response['qty'] = $item->qty;
		$response['dateDelivered'] =$item->dateDelivered;
		$response['remarks'] = $item->remarks;
		$response['remQty'] = $item->remQty;
		$response['remQty2'] = $item->remQty2;


    echo json_encode($response);
}else{
	$response['success'] = 0;
    $response['message'] = "Its not working.";

    echo json_encode($response);
}
?>