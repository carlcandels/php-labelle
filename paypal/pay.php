<?php
use PayPal\Api\Payment; 
use PayPal\Api\PaymentExecution;
	require '../src/start.php';
	include_once '../classes/database2.php';
	include_once '../classes/order.php';
	include_once '../classes/orderbatch.php';
	include_once '../classes/item.php';
	include_once '../classes/shipaddress.php';

	$database = new Database();
	$db = $database->getConnection();

	$order = new Order($db);
	$orderbatch = new OrderBatch($db);
	$item = new Item($db);
	$shipaddress = new ShipAddress($db);


	if(isset($_GET['approved']))
	{
		$approved = $_GET['approved'] == 'true';

		if($approved)
		{
			$payerId = $_GET['PayerID'];

			//Get payment_id from database
			$hash = $_SESSION['paypal_hash'];

			$paymentId = $_SESSION['payment_id'];

			//get the paypal payment
			$payment = Payment::get($paymentId, $api);

			$execution = new PaymentExecution();
			$execution->setPayerId($payerId);

			//execute paypal paymen (charge)
			$payment->execute($execution, $api);

			$stack = isset($_SESSION['paypal_pay']) ? $_SESSION['paypal_pay'] : null;

			$subtotal = $stack[0];
			$shipping_fee = $stack[1];
			$amountDue = $stack[2];

			$dateNow = date("m/d/Y h:i:sa");

			$flag = "1";
			$userID = $_SESSION['user_id'];

			$order->orderDate = $dateNow;
			$order->totalAmount = $amountDue;
			$order->flag = $flag;
			$order->amountTend = $amountDue;
			$order->userID = $userID;
			$order->paymentID = $paymentId;
			$order->hash = $hash;
			$order->shipping_fee =  $shipping_fee;
			$order->complete = "1";	

			//add order || execute insert order
			$orderID = $order->insert();


			//get shipping address
			$stack1 = isset($_SESSION['shipping']) ? $_SESSION['shipping'] : null;

			if ($stack1!=null)
            {
                foreach ($stack1 as $shipAddress) {
                $fullname = $shipAddress[0];
                $com_address = $shipAddress[1];
                $region = $shipAddress[2];
                $email = $shipAddress[3];
                $regionID = $shipAddress[4];

                $shipaddress->fullname = $fullname;
                $shipaddress->com_address = $com_address;
                $shipaddress->region = $region;
                $shipaddress->email = $email;
                $shipaddress->regionID = $regionID;
                $shipaddress->orderID = $orderID;

                $shipaddress->insert();
                }
            }

			//get cart
			$stack = isset($_SESSION['cart']) ? $_SESSION['cart'] : null;

			if ($stack!=null)
            {
                foreach ($stack as $cart) {
                $itemCode = $cart[0];
                $qty = $cart[2];
                $price = $cart[1];

                $orderbatch->itemCode = $itemCode;
                $orderbatch->orderID = $orderID;
                $orderbatch->qty = $qty;
                $orderbatch->price = $price;

                $orderbatch->insert();

                $item->itemCode = $itemCode;
                $item->readOne();
                $itemRemQty = $item->remQty2;

                $itemRemQty = $itemRemQty - $qty;

                $item->remQty2 = $itemRemQty;

                $item->updateRemQty2();
                }
            }

			
			//unset hash
			unset($_SESSION['cart']);
			unset($_SESSION['payment_flag']);
			unset($_SESSION['payment_id']);
			unset($_SESSION['paypal_hash']);
			unset($_SESSION['shipping']);

		header('Location: ../user/orderlist.php');

		}
		else
		{
			header('Location: ../user/paymentinfo.php');
		}
	}
	
?>;
