<?php
	session_start();	
	$itemCode = isset($_GET['itemCode']) ? $_GET['itemCode'] : 0;
	$itemPrice = isset($_GET['itemPrice']) ? $_GET['itemPrice'] : 0;
	$qty = isset($_GET['qty']) ? $_GET['qty'] : 0;

	$stack = $_SESSION['cart'];

	$arr = array();
	array_push($arr, $itemCode);
	array_push($arr, $itemPrice);
	array_push($arr, $qty);

	$stack = removeElementWithValue($stack, 0, $itemCode);

	function removeElementWithValue($array, $key, $value){
	     foreach($array as $subKey => $subArray){
	          if($subArray[$key] == $value){
	               unset($array[$subKey]);
	          }
	     }
	     return $array;
	}
	
	$_SESSION['cart'] = $stack;

	header("Location: paymentinfo.php");
?>