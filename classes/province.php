<?php
class Province{
    
  // database connection and table name
    private $conn;
    private $table_name = "refprovince";
 
    // object properties
    public $id;
    public $psgcCode;
    public $provDesc;
    public $regCode;
    public $provCode;
    
    public function __construct($db){
        $this->conn = $db;
    }    
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name . " WHERE regCode = ?";  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->regCode);
        $stmt->execute();
 
        return $stmt;
    }
    
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE id = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->id = $row['id'];
        $this->psgcCode = $row['psgcCode'];
        $this->regDesc = $row['provDesc'];
        $this->regCode = $row['regCode'];
        $this->regCode = $row['provCode'];
    }


}

?>