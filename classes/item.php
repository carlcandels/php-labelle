<?php
class Item{
    
  // database connection and table name
    private $conn;
    private $table_name = "item";
 
    // object properties
    public $itemCode;
    public $itemDesc;
    public $retailPrice;
    public $qty;
    public $dateDelivered;
    public $remarks;
    public $shopID;
    public $remQty;
    public $category;
    public $img;
    public $keywords;
    public $remQty2;
    public $color;
    public $size;
 
    public function __construct($db){
        $this->conn = $db;
    }
    
      // create chapter
    function insert(){
 
    
 
        //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    itemCode = ?, itemDesc = ?, retailPrice = ? qty = ?, dateDelivered = ?, remarks = ?, shopID = ?, remQty = ?, category = ?, img = ?, keywords = ?, remQty2 = ?, color = ?, size = ?";
 
        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(1, $this->itemCode);
        $stmt->bindParam(2, $this->itemDesc);
        $stmt->bindParam(3, $this->retailPrice);
        $stmt->bindParam(4, $this->qty);
        $stmt->bindParam(5, $this->dateDelivered);
        $stmt->bindParam(6, $this->remarks);
        $stmt->bindParam(7, $this->shopID);
        $stmt->bindParam(8, $this->remQty);
        $stmt->bindParam(9, $this->category);
        $stmt->bindParam(10, $this->img);
        $stmt->bindParam(11, $this->keywords);
        $stmt->bindParam(12, $this->remQty2);
        $stmt->bindParam(13, $this->color);
        $stmt->bindParam(14, $this->size);

        if($stmt->execute()){
            return true;
        }else{
            return false;
        }

    }
    
    
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }

    function readImg()
    {
        $query = "SELECT img FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
    function readAllPage($page, $from_record_num, $records_per_page){
 
        $query = "SELECT * FROM " . $this->table_name . " LIMIT {$from_record_num}, {$records_per_page}";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }
       
       // used for paging chapter
    public function countAll(){
     
        $query = "SELECT itemCode FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE itemCode = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->itemCode);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->itemCode = $row['itemCode'];
        $this->itemDesc = $row['itemDesc'];
        $this->retailPrice = $row['retailPrice'];
        $this->qty = $row['qty'];
        $this->dateDelivered = $row['dateDelivered'];
        $this->remarks = $row['remarks'];
        $this->shopID = $row['shopID'];
        $this->remQty = $row['remQty'];
        $this->category = $row['category'];
        $this->img = $row['img'];
        $this->keywords = $row['keywords'];
        $this->remQty2 = $row['remQty2'];
        $this->color = $row['color'];
        $this->size = $row['size'];
    }

    
    function updateRemQty2(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    remQty2 = :remQty2
                WHERE
                    itemCode = :itemCode";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':itemCode', $this->itemCode);
        $stmt->bindParam(':remQty2', $this->remQty2);
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    itemDesc = :itemDesc,
                    retailPrice = :retailPrice,
                    qty = :qty,
                    dateDelivered = :dateDelivered,
                    remarks = :remarks,
                    shopID = :shopID,
                    remQty = :remQty,
                    category = :category,
                    img = :img,
                    keywords = :keywords,
                    remQty2 = :remQty2,
                    color = :color,
                    size = :size
                WHERE
                    itemCode = :itemCode";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':itemCode', $this->itemCode);
        $stmt->bindParam(':itemDesc', $this->itemDesc);
        $stmt->bindParam(':retailPrice', $this->retailPrice);
        $stmt->bindParam(':qty', $this->qty);
        $stmt->bindParam(':dateDelivered', $this->dateDelivered);
        $stmt->bindParam(':remarks', $this->remarks);
        $stmt->bindParam(':shopID', $this->shopID);
        $stmt->bindParam(':remQty', $this->remQty);
        $stmt->bindParam(':category', $this->category);
        $stmt->bindParam(':img', $this->img);
        $stmt->bindParam(':keywords', $this->keywords);
        $stmt->bindParam(':remQty2', $this->remQty2);
        $stmt->bindParam(':color', $this->color);
        $stmt->bindParam(':size', $this->size);

   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>