<?php
class Category{
  // database connection and table name
    private $table_name = "category";
    private $conn;
 
    // object properties
    public $categoryID;
    public $categoryName;
    public $flag;
 
    public function __construct($db){
        $this->conn = $db;
    }
    
      // create chapter
    function insert(){
 
    //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    categoryID = :categoryID, categoryName = :categoryName, flag = :flag";
 
        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(':categoryID', $this->categoryID);
        $stmt->bindParam(':categoryName', $this->categoryName);
        $stmt->bindParam(':flag', $this->flag);

 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
 
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
       
       // used for paging chapter
    public function countAll(){
     
        $query = "SELECT categoryID FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE categoryID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->categoryID);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->categoryID = $row['categoryID'];
        $this->categoryName = $row['categoryName'];
        $this->flag = $row['flag'];
    }
    
 
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    flag = :flag,
                    categoryName = :categoryName
                WHERE
                    categoryID = :categoryID";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':categoryName', $this->categoryName);
        $stmt->bindParam(':categoryID', $this->categoryID);
        $stmt->bindParam(':flag', $this->flag);
   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    function isExisting(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE categoryID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->categoryID);
        $stmt->execute();
        
        $num = $stmt->rowCount();

        if($num>0)
            return true;
        else
            return false;
    }

}

?>