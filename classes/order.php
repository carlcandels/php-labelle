<?php
class Order{
    
  // database connection and table name
    private $conn;
    private $table_name = "order_online";
 
    // object properties
    public $orderID;
    public $orderDate;
    public $totalAmount;
    public $flag;
    public $amountTend;
    public $userID;
    public $paymentID;
    public $hash;
    public $complete;
    public $shipping_fee;
 
    public function __construct($db){
        $this->conn = $db;
    }
    

    function insert(){
 
            //write query
        $query = "INSERT INTO " . $this->table_name . " SET orderDate = :orderDate, totalAmount = :totalAmount, flag = :flag, amountTend = :amountTend, userID = :userID, paymentID = :paymentID, hash = :hash, complete = :complete, shipping_fee = :shipping_fee";

        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(':orderDate', $this->orderDate);
        $stmt->bindParam(':totalAmount', $this->totalAmount);
        $stmt->bindParam(':flag', $this->flag);
        $stmt->bindParam(':amountTend', $this->amountTend);
        $stmt->bindParam(':userID', $this->userID);
        $stmt->bindParam(':paymentID', $this->paymentID);
        $stmt->bindParam(':hash', $this->hash);
        $stmt->bindParam(':complete', $this->complete);
        $stmt->bindParam(':shipping_fee', $this->shipping_fee);

 
        if($stmt->execute()){
            return $this->conn->lastInsertId();
        }else{
            return false;
        }
    }
       
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
       
    public function countAll(){
     
        $query = "SELECT orderID FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE orderID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->orderID);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->orderDate = $row['orderDate'];
        $this->totalAmount = $row['totalAmount'];
        $this->flag = $row['flag'];
        $this->amountTend = $row['amountTend'];
        $this->userID = $row['userID'];
        $this->paymentID = $row['paymentID'];
        $this->hash = $row['hash'];
        $this->complete = $row['complete'];
        $this->shipping_fee = $row['shipping_fee'];
    }

    function readUserOrder(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE userID = ?";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->userID);
        $stmt->execute();
     
        return $stmt;
    }

    function readHash(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE hash = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->hash);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->orderDate = $row['orderDate'];
        $this->totalAmount = $row['totalAmount'];
        $this->flag = $row['flag'];
        $this->amountTend = $row['amountTend'];
        $this->userID = $row['userID'];
        $this->paymentID = $row['paymentID'];
        $this->hash = $row['hash'];
        $this->complete = $row['complete'];
        $this->shipping_fee = $row['shipping_fee'];   
    }
    
 
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    orderDate = :orderDate,
                    totalAmount = :totalAmount,
                    flag = flag,
                    amountTend = :amountTend,
                    userID = :userID,
                    paymentID = :paymentID, 
                    hash = :hash,
                    complete = :complete,
                    shipping_fee = :shipping_fee
                WHERE
                    orderID = :orderID";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':orderDate', $this->orderDate);
        $stmt->bindParam(':totalAmount', $this->totalAmount);
        $stmt->bindParam(':flag', $this->flag);
        $stmt->bindParam(':amountTend', $this->amountTend);
        $stmt->bindParam(':userID', $this->userID);
        $stmt->bindParam(':paymentID', $this->paymentID);
        $stmt->bindParam(':hash', $this->hash);
        $stmt->bindParam(':complete', $this->complete);
        $stmt->bindParam(':orderID', $this->shopID);
        $stmt->bindParam(':shipping_fee', $this->shopID);
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>