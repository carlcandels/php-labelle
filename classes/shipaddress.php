<?php
class ShipAddress{
    
  // database connection and table name
    private $conn;
    private $table_name = "ship_address";
 
    // object properties
    public $id;
    public $fullname;
    public $com_address;
    public $region;
    public $email;
    public $regionID;
    public $orderID;
 
    public function __construct($db){
        $this->conn = $db;
    }
    

    function insert(){
 
            //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    fullname = :fullname, com_address = :com_address, region = :region, email = :email, regionID = :regionID, orderID = :orderID";

        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(':fullname', $this->fullname);
        $stmt->bindParam(':com_address', $this->com_address);
        $stmt->bindParam(':region', $this->region);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':regionID', $this->regionID);
        $stmt->bindParam(':orderID', $this->orderID);

 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
       
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
       
    public function countAll(){
     
        $query = "SELECT id FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE id = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->orderDate = $row['fullname'];
        $this->totalAmount = $row['com_address'];
        $this->flag = $row['region'];
        $this->amountTend = $row['email'];
        $this->userID = $row['regionID'];
        $this->paymentID = $row['orderID'];
    }

     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    fullname = :fullname,
                    com_address = :com_address,
                    region = :region,
                    email = :email,
                    regionID = :regionID,
                    orderID = :orderID
                WHERE
                    id = :id";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':fullname', $this->fullname);
        $stmt->bindParam(':com_address', $this->com_address);
        $stmt->bindParam(':region', $this->region);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':regionID', $this->regionID);
        $stmt->bindParam(':orderID', $this->orderID);
        $stmt->bindParam(':id', $this->id);
   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>