<?php
class Regions{
    
  // database connection and table name
    private $conn;
    private $table_name = "refregion";
 
    // object properties
    public $id;
    public $psgcCode;
    public $regDesc;
    public $regCode;
    public $shipping_fee;
    
    public function __construct($db){
        $this->conn = $db;
    }    
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE id = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->id = $row['id'];
        $this->psgcCode = $row['psgcCode'];
        $this->regDesc = $row['regDesc'];
        $this->regCode = $row['regCode'];
        $this->shipping_fee = $row['shipping_fee'];
    }


}

?>