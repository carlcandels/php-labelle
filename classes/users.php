<?php
class Users{
  // database connection and table name
    private $table_name = "users";
    private $conn;
 
    // object properties
    public $userID;
    public $lname;
    public $fname;
    public $mname;
    public $email;
    public $username;
    public $password;
    public $restriction;
 
    public function __construct($db){
        $this->conn = $db;
    }
    
      // create chapter
    function insert(){
 
    //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    lname = :lname, fname = :fname, mname = :mname, email = :email, username = :username, password = :password, restriction=:restriction";
 
        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(':lname', $this->lname);
        $stmt->bindParam(':fname', $this->fname);
        $stmt->bindParam(':mname', $this->mname);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':restriction', $this->restriction);

 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
 
    }
    function login(){
        $query = "SELECT * FROM " . $this->table_name . " WHERE username = ? AND password = ?";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->username);
        $stmt->bindParam(2, $this->password);

        $stmt->execute();

        $row = $stmt->rowCount();

        return $row;
    }
    
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
    function readAllPage($page, $from_record_num, $records_per_page){
 
        $query = "SELECT
                    cID,cTitle, cDesc
                FROM
                    " . $this->table_name . "
                LIMIT
                    {$from_record_num}, {$records_per_page}";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }
       
       // used for paging chapter
    public function countAll(){
     
        $query = "SELECT shopID FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE userID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->userID);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->userID = $row['userID'];
        $this->lname = $row['lname'];
        $this->fname = $row['fname'];
        $this->mname = $row['mname'];
        $this->email = $row['email'];
        $this->username = $row['username'];
        $this->password = $row['password'];
        $this->restriction = $row['restriction'];
    }
    
 
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    shopName = :lname,
                    shopAddress = :fname,
                    shopContact = mname,
                    shopLogo = :email,
                    flag = :username,
                    lname = :password,
                    restriction = :restriction
                WHERE
                    userID = :userID";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':lname', $this->lname);
        $stmt->bindParam(':fname', $this->fname);
        $stmt->bindParam(':mname', $this->mname);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':password', $this->password);
        $stmt->bindParam(':userID', $this->userID);
        $stmt->bindParam(':restriction', $this->restriction);
   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    function getUserID()
    {
        $query = "SELECT * FROM " . $this->table_name . " WHERE username = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->username);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->userID = $row['userID'];
        $this->lname = $row['lname'];
        $this->fname = $row['fname'];
        $this->mname = $row['mname'];
        $this->email = $row['email'];
        $this->username = $row['username'];
        $this->password = $row['password'];
    }
}x

?>