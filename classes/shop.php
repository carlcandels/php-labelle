<?php
class Shop{
    
  // database connection and table name
    private $conn;
    private $table_name = "shop";
 
    // object properties
    public $shopID;
    public $shopName;
    public $shopAddress;
    public $shopContact;
    public $shopLogo;
    public $flag;
    public function __construct($db){
        $this->conn = $db;
    }
    
      // create chapter
    function insert(){
 
    
 
        //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    shopID = ?, shopName = ?, shopAddress = ? shopContact = ?, shopLogo = ?, flag = ?";
 
        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(1, $this->shopID);
        $stmt->bindParam(2, $this->shopName);
        $stmt->bindParam(3, $this->shopAddress);
        $stmt->bindParam(4, $this->shopContact);
        $stmt->bindParam(5, $this->shopLogo);
        $stmt->bindParam(6, $this->flag);

 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
       
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }

    function readLogo()
    {
        $query = "SELECT shopLogo FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
    function readAllPage($page, $from_record_num, $records_per_page){
 
        $query = "SELECT
                    *
                FROM
                    " . $this->table_name . "
                LIMIT
                    {$from_record_num}, {$records_per_page}";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        return $stmt;
    }
       
       // used for paging chapter
    public function countAll(){
     
        $query = "SELECT shopID FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE shopID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->shopID);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->shopName = $row['shopName'];
        $this->shopAddress = $row['shopAddress'];
        $this->shopContact = $row['shopContact'];
        $this->shopLogo = $row['shopLogo'];
        $this->flag = $row['flag'];
    }
    
 
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    shopName = :shopName,
                    shopAddress = :shopAddress,
                    shopContact = shopContact,
                    shopLogo = :shopLogo,
                    flag = :flag
                WHERE
                    shopID = :shopID";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':shopName', $this->shopName);
        $stmt->bindParam(':shopAddress', $this->shopAddress);
        $stmt->bindParam(':shopContact', $this->shopContact);
        $stmt->bindParam(':shopLogo', $this->shopLogo);
        $stmt->bindParam(':flag', $this->flag);
        $stmt->bindParam(':lname', $this->lname);
        $stmt->bindParam(':fname', $this->fname);
        $stmt->bindParam(':mname', $this->mname);
        $stmt->bindParam(':shopID', $this->shopID);
   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>