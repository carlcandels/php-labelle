<?php
class OrderBatch{
    
  // database connection and table name
    private $conn;
    private $table_name = "order_batch";
 
    // object properties
    public $orderbatchID;
    public $orderID;
    public $itemCode;
    public $qty;
    public $price;
 
    public function __construct($db){
        $this->conn = $db;
    }
    
      // create chapter
    function insert(){
 
            //write query
        $query = "INSERT INTO
            " . $this->table_name . "
                SET
                    orderID = ?, itemCode = ?, qty = ?, price = ?";
 
        $stmt = $this->conn->prepare($query);
 
        $stmt->bindParam(1, $this->orderID);
        $stmt->bindParam(2, $this->itemCode);
        $stmt->bindParam(3, $this->qty);
        $stmt->bindParam(4, $this->price);

 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
       
 
    // used by select drop-down list
    function readAll(){
        //select all data
        $query = "SELECT * FROM " . $this->table_name;  
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
 
        return $stmt;
    }
    
       
       // used for paging chapter
    public function countAll(){
     
        $query = "SELECT orderbatchID FROM " . $this->table_name . "";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
     
        $num = $stmt->rowCount();
     
        return $num;
    }
    
     function readOne(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE orderbatchID = ? LIMIT 0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->orderbatchID);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->orderID = $row['orderID'];
        $this->itemCode = $row['itemCode'];
        $this->qty = $row['qty'];
        $this->price = $row['price'];
    }

    function readAllOrder(){
 
        $query = "SELECT * FROM " . $this->table_name . " WHERE orderID = ?";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->orderID);
        $stmt->execute();
     
        return $stmt;
    }
    
 
    
     function update(){
 
        $query = "UPDATE " . $this->table_name . "
                SET
                    orderID = :orderID,
                    itemCode = :itemCode,
                    qty = qty,
                    price = :price
                WHERE
                    orderbatchID = :orderbatchID";
     
        $stmt = $this->conn->prepare($query);
     
        $stmt->bindParam(':orderID', $this->orderID);
        $stmt->bindParam(':itemCode', $this->itemCode);
        $stmt->bindParam(':qty', $this->qty);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':orderbatchID', $this->orderbatchID);
   
      
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>