<?php
	session_start();
	use PayPal\Rest\ApiContext;
	use PayPal\Auth\OAuthTokenCredential;


	require __DIR__ . '/../vendor/autoload.php';
	include_once '../classes/database2.php';
	include_once '../classes/users.php';


	$api = new ApiContext(
		new OAuthTokenCredential(
			'AWdmojRGpF1jdT919OlGZMiBCpwfGm03WRMryHBL5CMOXC03yUxQcxrIyVqjnO1oO3l7TfdHG7j8fY0m',
			'EMOuJ_Gz3D68NJ-sMrzmx82K2DJ3piaTOkoUC2ROvWUptf9ssccOZl26tjVc0YYC__OKnStXvo1oDvwa'
			)
		);	

	$api->setConfig([
		'mode' => 'sandbox',
		'http.ConnectionTimeOut' => 40,
		'log.LogEnabled' => false,
		'log.FileName' => '',
		'log.LogLevel' => 'FINE',
		'validation.level' => 'log'
		]);

	$database = new Database();
    $db = $database->getConnection();

    $users = new Users($db);

    $users->userID = $_SESSION['user_id'];
    $stmt = $users->readOne();

?>